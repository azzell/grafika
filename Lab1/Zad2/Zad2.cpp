﻿// zaj2opgl.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "math.h"
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
#define GL_PI 3.1415f

///////////////////////////////////////////////////////////
// Wywoływana w celu przerysowania sceny
void RenderScene(void) {
	// Wyczyszczenie okna aktualnym kolorem czyszczcym   
	glClear(GL_COLOR_BUFFER_BIT);
	// Aktualny kolor rysujcy - czerwony  
	//           R     G     B  
	glColor3f(1.0f, 1.0f, 0.0f);
	glBegin(GL_POLYGON);
	GLfloat angle, x, y;
	for (angle = 0.0f; angle < (4.25f*GL_PI); angle += (GL_PI / 5.0f))
	{
		// Wyliczenie współrzędnych x i y kolejnego wierzchołka
		x = 50.0f*sin(angle);
		y = 50.0f*cos(angle);
		// Wybieranie koloru - zielony lub czerwony

		// Inkrementacja zmiennej okrelającej rodzaj koloru

		// Definiowanie kolejnego wierzchołka w wachlarzu trójkątów
		glVertex2f(x, y);
	}



	//glVertex2f(10.0, -50.0);//1
	//glVertex2f(40.0, -25.0);//2
	//glVertex2f(50.0, 0.0);//3
	//glVertex2f(40.0, 25.0);//4
	//glVertex2f(10.0, 50.0);//5
	//glVertex2f(-10.0, 50.0);//6
	//glVertex2f(-40.0, 25.0);//7
	//glVertex2f(-50.0, 0.0);//8
	//glVertex2f(-40.0, -25.0);//9
	//glVertex2f(-10.0, -50.0);//10

	glEnd();

	// Narysowanie prostokta wypełnionego aktualnym kolorem  
	//glRectf(-25.0f, 25.0f, 25.0f, -25.0f);
	// Wysłanie polece do wykonania     
	glFlush();
}
///////////////////////////////////////////////////////////
// Konfiguracja stanu renderowania  
void SetupRC(void) {
	// Ustalenie niebieskiego koloru czyszczcego     
	glClearColor(0.0f, 1.0f, 1.0f, 1.0f);
}
///////////////////////////////////////////////////////////
// Wywoływana przez bibliotek GLUT w przypadku zmiany rozmiaru okna
void ChangeSize(int w, int h) {
	GLfloat aspectRatio;
	// Zabezpieczenie przed dzieleniem przez zero  
	if (h == 0)   h = 1;
	// Ustawienie wielkości widoku na równą wielkości okna     
	glViewport(0, 0, w, h);
	// Ustalenie układu współrzędnych  
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	// Wyznaczenie przestrzeni ograniczającej (lewy, prawy, dolny, górny, bliski, odległy)  
	aspectRatio = (GLfloat)w / (GLfloat)h;
	if (w <= h)    glOrtho(-100.0, 100.0, -100 / aspectRatio, 100.0 / aspectRatio, 1.0, -1.0);
	else    glOrtho(-100.0 * aspectRatio, 100.0 * aspectRatio, -100.0, 100.0, 1.0, -1.0);
	glMatrixMode(GL_MODELVIEW);  glLoadIdentity();
}
///////////////////////////////////////////////////////////
// Główny punkt wejcia programu
int main(int argc, char* argv[]) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(800, 800);
	glutCreateWindow("GLPolygon");
	glutDisplayFunc(RenderScene);
	glutReshapeFunc(ChangeSize);
	SetupRC();
	glutMainLoop();
	return 0;
}
